package com.slavik.sharedpreferences;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //SharedPreferences preferences = this.getPreferences(Context.MODE_PRIVATE);

        //SharedPreferences.Editor editor = preferences.edit();
        //editor.putLong("Time", System.nanoTime());
        //editor.commit();

        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment()).commit();

    }


}
