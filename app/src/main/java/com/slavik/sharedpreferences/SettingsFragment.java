package com.slavik.sharedpreferences;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.widget.Toast;


public class SettingsFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences.OnSharedPreferenceChangeListener listener =
                new SharedPreferencesListener();

        PreferenceManager.getDefaultSharedPreferences(getActivity().
                getApplicationContext()).registerOnSharedPreferenceChangeListener(listener);

        addPreferencesFromResource(R.xml.preferences);
    }


    class SharedPreferencesListener implements SharedPreferences.OnSharedPreferenceChangeListener {

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            String newValue = sharedPreferences.getAll().get(key).toString();
            Toast.makeText(getActivity(), key + " has changed to " + newValue, Toast.LENGTH_SHORT).show();
        }
    }

}
